\documentclass[]{beamer}
\usepackage{fontspec,polyglossia,xunicode,hyperref,csquotes}
\usepackage{graphicx}
\graphicspath{{img/}{}}
\usetheme[]{Boadilla}

\newcommand\pointmedian{%
 \kern-0.25em\textperiodcentered\kern-0.25em}
\catcode`·=\active
\def·{\pointmedian}
\newenvironment{slide}{%
  \begin{frame}
    <presentation>\mode<presentation>{\frametitle{\insertsubsection}}%
  }%
{\end{frame}}
\newenvironment{slide*}{\begin{frame}<presentation>[<1>]\mode<presentation>{\frametitle{\insertsubsection}}}{\end{frame}}


\beamerdefaultoverlayspecification{<+->}
\setmainfont{Linux Libertine O}
\setmainlanguage{french}
\setotherlanguage{english}
\usepackage{minted}
\newcounter{code}
\resetcounteronoverlays{code}
\usepackage{hyperref}
\hypersetup{bookmarksdepth=6}
\renewcommand{\thecode}{\arabic{code}}
\newenvironment{attention}%
{\begin{alertblock}{Attention}}%
{\end{alertblock}}
\newcommand{\code}[3]{%
  \stepcounter{code}%
  \begin{block}{Code \thecode : #3}%
    \begin{english}\footnotesize%
      \inputminted[linenos=true,breaklines=true]{#2}{code/#1}%
    \end{english}%
  \end{block}
}
\newcommand{\extension}[1]{.\texttt{#1}}
\newcommand{\codeinline}[1]{\texttt{#1}}
\newcommand{\meta}[1]{\texttt{<#1>}}
\newcommand{\logiciel}[1]{\emph{#1}}
\newcommand{\fichier}[1]{\texttt{#1}}
\setcounter{tocdepth}{1}
\AtBeginSection{
  \mode<presentation>{\frame{\sectionpage}}
}
\setbeamertemplate{footline}[frame number]
\setbeamertemplate{navigation symbols}{}%remove navigation symbols

\usepackage{tikz}

\usepackage[style=verbose]{biblatex}
\bibliography{biblio.bib}

\renewcommand{\newunitpunct}[0]{\addcomma\addspace}
\renewcommand{\subtitlepunct}[0]{\adddot\addspace}
\author{Maïeul Rouquette}
\date{Stage \LaTeX\ à Dunkerque}
\title{Gérer son historique de ﬁchiers \LaTeX\  avec Git}
\institute{Université de Lausanne --- IRSB}
\date{12 juin 2019}


\usepackage[]{qrcode}
\newcommand{\linkintitlepage}[1]{%
  \bgroup
  \tiny
  \vfill
  \parbox[c][2cm]{0.8\textwidth}{
    \url{#1}
    \vfill
    Licence Creative Commons France 3.0 - Paternité - Partage à l'identique
  }
  \hfill \qrcode{#1}
  \egroup
}

\begin{document}

\begin{frame}
  \titlepage
  \linkintitlepage{https://geekographie.maieul.net/231}
\end{frame}


\begin{frame}
  \tableofcontents
\end{frame}

\section{Gestionnaire de version, quesaco?}
\subsection{Un gestionnaire de version, pour quoi faire?}
\begin{slide}
  \begin{itemize}
    \item{Écrire un texte, créer ses propres macros, personnaliser \LaTeX, créer ses packages impliquent}:
      \begin{itemize}
        \item Une démarche progressive
        \item Des risques d'erreur
        \item Parfois un travail collaboratif avec d'autres personnes
        \item Parfois la nécessité d'essayer de nouvelles choses sans être certain du résultat
      \end{itemize}
    \item Il faut donc pouvoir
      \begin{itemize}
        \item Avoir un historique des fichiers
        \item Marquer les grandes étapes du travail
        \item Synchroniser facilement les fichiers et l'historique avec d'autres personnes
        \item Proposer et accepter facilement des nouvelles modifications
        \item Gérer plusieurs versions parallèles d'un même projet afin d'avancer de façons indépendantes sur des points indépendants
      \end{itemize}
    \item C'est le but d'un logiciel de \alert{versionnement} (ou versionnage) ou \alert{gestionnaire de version}
  \end{itemize}
\end{slide}
\subsection{Quelques autres intérêts d'un gestionnaire de version}
\begin{slide}
  \begin{itemize}
    \item \alert{En informatique} : souvent couplé avec une \alert{forge logicielle}
      \begin{itemize}
        \item  Gestion de tickets
        \item  Outils automatisés de test
      \end{itemize}
    \item \alert{En rédactionnel} : pousse à travailler étape par étape, et donc à ne pas se disperser (ex : ma thèse de doctorat)
    \item \alert{En général} : outil simple de sauvegarde
  \end{itemize}
\end{slide}

\subsection{Gestionnaire de version centralisé vs décentralisé}
\begin{slide}
  \begin{itemize}
    \item Centralisé (CVS, SVN)
      \begin{itemize}
        \item Un serveur contient l'\alert{historique unique} des modifications
        \item Des utilisateurs, depuis leurs ordinateurs, envoient leurs modifications, ce qui complète l'historique
        \item \alert{Avantages}
          \begin{itemize}
            \item Simple d'approche
            \item Économe en ressources
          \end{itemize}
        \item \alert{Inconvénients}
          \begin{itemize}
            \item  Peu souple, plus complexe pour gérer des versions parallèles
            \item  Nécessite une connexion internet pour travailler
          \end{itemize}
      \end{itemize}
    \item Décentralisé (Git, Mercurial)
      \begin{itemize}
        \item Un historique (ou plus) conservé sur l'ordinateur de chaque utilisateur
        \item \alert{Historiques synchronisés}, soit directement, soit, le plus souvent, via un serveur de référence
        \item \alert{Avantages}
          \begin{itemize}
            \item Pas besoin de connexion internet pour travailler
            \item Souple, puissant
          \end{itemize}
        \item \alert{Inconvénients}
          \begin{itemize}
            \item  Plus complexe à appréhender
            \item  Plus gourmand en ressources
          \end{itemize}
      \end{itemize}
  \end{itemize}
\end{slide}
\subsection{Un peu de terminologie autour de \logiciel{Git}}
\begin{slide}
  \begin{itemize}
    \item \logiciel{Git} est  un \alert{logiciel de versionnement} pouvant s'exécuter :
      \begin{itemize}
        \item En ligne de commande
        \item Par interface graphique spécifique (\url{https://git-scm.com/downloads/guis})
        \item Dans l'interface de certains éditeurs de texte (\logiciel{Atom},  \logiciel{Vim}, \logiciel{Emacs})
        \item En interface web
        \item \alert{L'interface ligne des commande est la base, les autres interfaces n'ont pas toujours toutes les fonctionnalités}
      \end{itemize}
    \item \logiciel{Github} est une forge logicielle, \alert{propriétaire}
    \item \logiciel{Gitlab} est une forge logicielle, \alert{libre}, dont \logiciel{Framagit} est l'une des instances
  \end{itemize}
\end{slide}
\section{Versionner son travail individuel}

\subsection{Installation et configuration}

\begin{slide}
  \begin{itemize}
    \item Pour l'installation, suivre la procédure correspondant à votre système d'exploitation indiquée sur \url{https://git-scm.com/downloads}
      \begin{itemize}
        \item \alert{GNU/Linux} : avec gestionnaire de paquets
        \item \alert{Mac Os} : fichier d'installation à télécharger puis à exécuter
        \item  \alert{Windows} : idem, choisir les options par défaut, notamment
          \begin{itemize}
            \item \enquote{Windows Explorer Integration}
            \item \enquote{Use git from the Windows command Prompt}
            \item \enquote{Checkout Windows-style, commit Unix-style line endings}
          \end{itemize}
      \end{itemize}
    \item  Dans votre \logiciel{Terminal} (\logiciel{Git Bash} sous Windows), exécuter les commandes suivantes :
      \code{config}{bash}{Configuration de base de \logiciel{Git}}
  \end{itemize}
\end{slide}

\subsection{La notion de dépôt}
\begin{slide}
  \begin{itemize}
    \item Un  \alert{dépôt} contient l'historique du projet
    \item Il peut être \alert{local} ou \alert{distant}
    \item Un dépôt peut récupérer (\alert{\codeinline{pull}}) ou envoyer (\alert{\codeinline{push}}) de l'historique depuis/vers un autre dépôt (\alert{\codeinline{remote}})
  \end{itemize}
\end{slide}

\subsection{Création du dépôt local}

\begin{slide}
  \code{init}{bash}{Création d'un dépôt local}
\end{slide}

\subsection{Le \alert{commit}, élément de base}

\begin{slide}
  \begin{itemize}
    \item Un \alert{\emph{commit}} correspond à l'\alert{état des fichiers à un instant \emph{t}}
    \item Garder un historique avec \logiciel{git}, c'est garder une succession de \emph{commits}
    \item Dans un \emph{commit} sont stockés :
      \begin{itemize}
        \item L'état des fichiers à un instant \emph{t}
        \item Le moment du commit
        \item L'auteur du commit (identifié par nom et courriel)
        \item Un court message résumant les modifications apportées (par ex. \enquote{relecture du chap. 23})
        \item {Le ou les commit(s) parent(s)}
      \end{itemize}
    \item Un commit possède un identifiant unique, un \alert{\emph{hash}} de 40 caractères hexadecimaux
    \item Généralement, les huit premiers caractères du \emph{hash} suffisent à identifier un commit
  \end{itemize}
\end{slide}

\subsection{Quelques règles pour un bon \emph{commit}}
\begin{slide}
  \begin{itemize}
    \item Un \emph{commit} doit disposer d'un \alert{message clair et explicite} le résumant
    \item Il doit correspondre à un \alert{modification unitaire}
      \begin{itemize}
        \item  On ne mélangera pas relectures orthographiques, relectures de fond et corrections de macro dans un même commit
        \item Le cas échéant, il est possible de commiter partiellement une modification (\codeinline{git commit -p})
      \end{itemize}
    \item  En règle générale il ne faut versionner que les \alert{fichiers sources} (\extension{tex}, \extension{bib}) pas les fichiers finaux (\extension{pdf}) ni les fichiers intermédiaires (\extension{aux}, \extension{log}, etc.)
    \item  On crée un fichier \alert{.gitignore} dans le dépôt
    \item \url{https://github.com/github/gitignore/blob/master/TeX.gitignore}
  \end{itemize}
\end{slide}

\subsection{Commiter par la pratique}

\begin{slide}
  \begin{itemize}
    \item Dire quels fichiers on veut commiter
    \item Puis commiter
      \code{commit-base}{bash}{Syntaxe de base d'un premier commit}
      \code{commit-base-exemple}{bash}{Exemple de premier commit}
  \end{itemize}
\end{slide}

\begin{slide}
  \begin{alertblock}{Un oubli fréquent}
    Ne pas oublier le \codeinline{git add <fichier>}
  \end{alertblock}
  \begin{figure}
    \includegraphics[height=4\baselineskip]{etats.png}
    \caption{3 états dans Git
    (Schéma par Scott Chacon, sous licence CC-by-nc-sa 3.0)}
  \end{figure}
  \begin{alertblock}{Pour aller plus loin}
    Un raccourci possible \codeinline{git commit -a -m  "message"}\\
    Mais ce raccourci ne fonctionne que pour les fichiers déjà versionnés.
  \end{alertblock}
  \begin{alertblock}{Une petite précision}
    Un commit peut bien sûr concerner plus d'un fichier.
  \end{alertblock}
\end{slide}

\subsection{Quelques commandes utiles}

\begin{slide}
  \code{amend}{bash}{Corriger / compléter le dernier commit}
  \code{mv}{bash}{Déplacer ou renommer un fichier}
  \code{rm}{bash}{Effacer un fichier}
\end{slide}

\begin{slide}
  \code{diff}{diff}{Voir les différences entre l'état actuel du projet et le dernier \emph{commit}}
  \code{checkout}{bash}{Annuler les modifications d'un fichier}
  \code{checkout-commit}{bash}{Retrouver l'état des fichiers à un commit précis}
\end{slide}
\subsection{Tirer parti de l'historique}
\begin{slide}
  \code{log}{bash}{Afficher l'historique}
  \code{revert}{bash}{Annuler un commit précis}
  \begin{alertblock}{Un petit piège}
    Lorsqu'on fait un \codeinline{git revert}, Git propose de modifier le message du \emph{commit} d'annulation.

    Il ouvre pour cela l'éditeur \logiciel{Vim} (on peut configurer pour avoir un autre éditeur).

    Taper \codeinline{:wq} dans l'éditeur pour enregistrer le fichier et garder le message par défaut du commit.
  \end{alertblock}
\end{slide}

\begin{slide}
  \code{tag}{bash}{Marquer une étape du projet}
  \code{tag-exemple}{bash}{Marquer une étape du projet - exemple}
\end{slide}
\section{Travailler à plusieurs}

\subsection{Deux modalités de travail}

\begin{slide}
  \begin{itemize}
    \item  La souplesse de \logiciel{Git} permet une multitude de modalités de travail collaboratif
    \item Pour le présent exposé, nous présenterons deux modes simples et courants :
      \begin{itemize}
        \item Tout le monde sur un \alert{pied d'égalité} travaille sur la \alert{même branche}
        \item Des mainteneur·euse·s reçoivent des \alert{propositions de modification} de la part de contributeur·trice·s, en utilisant \alert{plusieurs branches}
      \end{itemize}
  \end{itemize}
  \begin{alertblock}{Précision technique}
    La gestion des droits d'accès ne dépend pas directement de \logiciel{Git} mais bien de la \logiciel{forge logicielle}.

    Ici nous fonctionnerons avec \logiciel{Gitlab}. Pour \logiciel{Github}, quelques adaptations seront nécessaires, mais les principes globaux restent les mêmes.
  \end{alertblock}
\end{slide}

\subsection{Une difficulté majeure}
\begin{slide}
  \begin{itemize}
    \item La \alert{principale difficulté} avec le travail collaboratif résulte de la \alert{gestion des conflits}, si deux personnes travaillent sur le même fichier.
    \item  Par \alert{conflit}, nous entendons le fait que deux personnes peuvent  \alert{modifier en parallèle}  les \alert{mêmes lignes} d'un fichier.
    \item Pour limiter les risques il est important, avant toute session de travail de \alert{récupérer} (\codeinline{pull}) l'état des fichiers distants.
    \item Dans certains cas toutefois, des conflits resteront. Il faudra alors \alert{les résoudre manuellement} en choisissant quelle version on retient.
  \end{itemize}
\end{slide}

\subsection{Fusion \emph{vs} rebasage}
\newcommand{\commit}[4]{
  \node[draw,  minimum height=5\baselineskip, text width=1.3cm] (#1) at (#2) {#3\\ #4\strut}
}
\newcommand{\relation}[2]{
  \draw[->] (#1) -- (#2)
}
\begin{slide}
\onslide<1->{
  La \alert{fusion} et le \alert{rebasage} sont deux manières différentes de gérer des divergences dans l'historique.
}
  \tiny
  \begin{overprint}

    \begin{block}{
        \only<1>{Étape 1 : Virginie travaille}
        \only<2>{Étape 2 : Paul et Virginie travaillent en parallèle}
        \only<3>{Étape 3 - Solution a. : Virginie fusionnent les branches}
        \only<4>{Étape 3 - Solution b. : Virginie rebase le travail de Paul}
      }
      \begin{tikzpicture}[xscale=1.75,yscale=1.5]
        \commit{A}{0,0}{Virginie}{Commit initial};
        \commit{B}{1,0}{Virginie}{Ajout intro.};
        \commit{C}{2,0}{Virginie}{Ajout chap. 1};
        \relation{A}{B};
        \relation{B}{C};

        \onslide<2-3>{
          \commit{D}{3,1}{Paul}{Corrections intro.};
          \commit{E}{4,1}{Paul}{Corrections chap. 1};
          \relation{C}{D};
          \relation{D}{E};

          \commit{F}{3,0}{Virginie}{Ajout chap. 2};
          \commit{G}{4,0}{Virginie}{Ajout chap. 3};
          \relation{C}{F};
          \relation{F}{G};
        }
        \onslide<3>{
          \commit{H}{5,0}{Virginie}{Intégration corrections de Paul};
          \relation{E}{H};
          \relation{G}{H};
        }
        \onslide<4>{
          \commit{I}{3,0}{Paul}{Corrections intro.};
          \commit{J}{4,0}{Paul}{Corrections chap. 1};
          \commit{K}{5,0}{Virginie}{Ajout chap. 2};
          \commit{L}{6,0}{Virginie}{Ajout chap. 3};
          \relation{C}{I};
          \relation{I}{J};
          \relation{J}{K};
          \relation{K}{L};
        }
      \end{tikzpicture}
    \end{block}
  \end{overprint}
\end{slide}
\begin{slide}
  \begin{itemize}
    \item On \alert{rebase} lorsqu'on récupère veut simplement s'actualiser par rapport à un \alert{dépôt distant}
    \item On \alert{fusionne} lorsqu'on veut garder trace du \alert{travail parallèle} notamment lorsque
      \begin{itemize}
        \item  On suit le modèle de relecture du travail des autres contributeur·trice·s
        \item  On crée des branches de développement pour  de nouvelles fonctionnalités sur une classe ou un package
      \end{itemize}
  \end{itemize}
\end{slide}
\begin{slide}
  \begin{alertblock}{Remarques}
    Il s'agit de \alert{bonnes pratiques} pas d'obligation. Certains projets préfèrent le rebasage systématique. Plus rarement la fusion systématique.

    \url{https://delicious-insights.com/fr/articles/bien-utiliser-git-merge-et-rebase/}

    Pour faciliter la tâche, nous allons \alert{configurer} git pour faire le bon choix par défaut.
  \end{alertblock}
    \code{config-fusion}{bash}{Configuration de \logiciel{Git} pour avoir une bonne politique de gestion de l'historique}
  \begin{alertblock}{Sur l'historique}
    Rebaser revient à \alert{réécrire l'historique}. Les commits rebasés changent de \alert{hash} et \alert{date}. Mais auteur, contenu et message restent identiques.
  \end{alertblock}
\end{slide}
\subsection{Fonctionnement monobranche par la pratique : création et configuration du dépôt distant}
\begin{slide}
  \begin{itemize}
    \item Dans \logiciel{Framagit} / \logiciel{Gitlab} créer un dépôt commun et donner les droits aux personnes concernées
      \begin{itemize}
        \item Sur la page d'accueil, une fois connecté, choisir \enquote{Nouveau projet} / \enquote{New project}
        \item Configurer le nom et la visibilité  du projet
        \item Créer un \fichier{README.md} minimal (cela crée un premier commit)
        \item Puis pour le projet, aller dans \enquote{Paramètres / Membres}  / \enquote{Parameters / members} et donner les droits de \enquote{Mainteneur} / \enquote{Maintener}
        \item Une autre solution, plus propre mais plus complexe, est de créer un groupe
      \end{itemize}
    \item Sur la page du projet, le bouton clone permet de connaitre l'url du projet distant à cloner
  \end{itemize}
\end{slide}
\subsection{Fonctionnement monobranche par la pratique : travail sur les dépôts locaux et synchronisation}

\begin{slide}
  \code{clone}{bash}{Clonage initial du dépôt distant}
  \begin{alertblock}{À propos de l'identification}
    Pour communiquer avec un dépôt distant, il faut la plupart du temps s'identifier.
    La méthode la plus basique est celle du login/mot de passe. Elle est utilisée lorsque l'url commence par \codeinline{https://}.

    Pour éviter de taper systématiquement son mot de passe, deux solutions :
    \begin{itemize}
      \item  Tirer profit du système \codeinline{credential} de \alert{Git} \url{https://git-scm.com/book/fr/v2/Utilitaires-Git-Stockage-des-identifiants}
      \item  Plus sécurisé, utiliser une clé \codeinline{ssh} (dans ce cas, l'url du dépôt distant est différente) \url{https://git-scm.com/book/fr/v2/Git-sur-le-serveur-Génération-des-clés-publiques-SSH}
    \end{itemize}
  \end{alertblock}
\end{slide}

\begin{slide}
  \code{commit-base}{bash}{Faire autant de \emph{commits} que nécessaire}
  \code{push}{bash}{Envoyer sur le serveur distant}
  \code{pull}{bash}{Récupérer du serveur distant}
  \code{push-tags}{bash}{Envoyer les tags sur le serveur distant}
\end{slide}
\begin{slide}
  \begin{alertblock}{Sur les divergences d'historique}
    \begin{itemize}
      \item<1-> Pour limiter les risques, \alert{toujours faire un \codeinline{git pull}} en début de session de travail
      \item<+-> Si quelqu'un a pushé sur le serveur distant depuis votre dernier \codeinline{pull}, nous ne pourrons pas pusher:
        \begin{itemize}
          \item<+-> Faire un \codeinline{git pull}
          \item<+-> Avec la config que nous avons effectuée plus haut, les modifications distantes seront \alert{rebasées} avant vos modifications locales
          \item<+-> Si les mêmes lignes ont été modifiées en local et à distance:
            \begin{itemize}
              \item<+-> Il faudra modifier les fichiers concernés
              \item<+-> Repérer les lignes encadrées par \codeinline{>>>>} et  \codeinline{<<<<} : ce sont les lignes qui posent problème
              \item<+-> Choisir la version à retenir
              \item<+-> Il existe des outils de \alert{gestion des conflits} qui permettent de comparer les versions et de choisir la bonne, plutôt que de modifier \enquote{à la main} les fichiers en conflits
              \item<+-> Puis commiter
            \end{itemize}
          \item<+-> Puis \codeinline{git push}
        \end{itemize}
    \end{itemize}
  \end{alertblock}
\end{slide}

\subsection{Fonctionnement multibranche par la pratique : configuration}

\begin{slide}
  \begin{itemize}
    \item Configurer le dépôt distant comme pour le travail monobranche, mais donner seulement les droits de \enquote{Développeur} / \enquote{Developer} aux personnes qui ne peuvent pas travailler directement sur la branche principale
    \item Puis cloner le dépôt distant en local
  \end{itemize}
\end{slide}
\subsection{Fonctionnement multibranche par la pratique : création et bascule de branche}

\begin{slide}
  \begin{alertblock}{Noms et politique de branche}
    La branche par défaut est la branche \alert{master}.

    Il est conseillé de créer une branche par bloc fonctionnel.

    On pourra aussi créer des branches pour préparer la sortie d'une nouvelle version.

    Normalement la branche \alert{master} doit toujours être opérationnelle.
  \end{alertblock}

  \code{branche-creer}{bash}{Créer une nouvelle branche et y basculer}
  \code{branche-lister}{bash}{Lister les branches}
  \code{branche-basculer}{bash}{Basculer vers une branche}
\end{slide}
\subsection{Fonctionnement multibranche par la pratique : gestion des branches distantes}


\begin{slide}
  \code{push-all}{bash}{Envoyer toutes les branches locales sur le dépôt distant}
  \code{push-branch}{bash}{Envoyer une branche locale sur le dépôt distant}

  \begin{alertblock}{Sur \alert{origin}}

    \alert{origin} est le nom du \alert{dépôt distant} automatiquement créé lors d'un \codeinline{git clone}.

    Il est possible d'avoir plusieurs dépôts distants, mais nous n'en parlerons pas ici.
  \end{alertblock}
\end{slide}
\begin{slide}

  \code{fetch-track}{bash}{Travailler pour la première fois sur une branche distante}
  \code{pull-branch}{bash}{Récupérer le dernier état de la branche distante}
\end{slide}

\subsection{Fonctionnement multibranche par la pratique : fusion de branches}

\begin{slide}
  \begin{itemize}
    \item Via \alert{\logiciel{Gitlab}} la personne qui propose une fonctionnalité fait une demande de \alert{fusion de branche}:
      \begin{itemize}
        \item  Se rendre sur le dépôt
        \item  Choisir dans le menu déroulant la branche à fusionner
        \item  Puis \enquote{Créer une demander de fusion} / \enquote{Create a merge request}
        \item  Ou bien, suivre le lien qui apparaît lorsqu'on fait un \codeinline{git pull}
      \end{itemize}
    \item Les mainteneur·euse·s du projet peuvent, à travers l'interface de \logiciel{Gitlab}:
      \begin{itemize}
        \item Voir les modifications proposées
        \item Le cas échéant, faire des commentaires / poser des questions
        \item Et même modifier la branche distante en y apportant des commits
        \item Puis accepter la fusion
      \end{itemize}
    \item On peut aussi fusionner les branches en local
      \code{merge}{bash}{Fusionner une branche en local}
  \end{itemize}
\end{slide}

\subsection{Fonctionnement multibranche par la pratique : suppression de branche}
\begin{slide}
  \code{branche-supprimer}{bash}{Supprimer une branche locale}
  \code{branche-supprimer-distante}{bash}{Supprimer une branche distante}
  \code{branche-supprimer-distante-bis}{bash}{Supprimer une branche distante (syntaxe alternative)}
\end{slide}

\section{Pour aller plus loin}
\subsection{Autres fonctions utiles}
\begin{slide}
  \begin{itemize}
    \item Les \alert{alias} pour avoir des raccourcis de commande
    \item Le \alert{prompt coloré et autocomplétant} pour y voir plus clair
    \item  La \alert{remise} (\codeinline{git stash}) pour mettre temporairement de côté un travail
    \item  Le \alert{rebasage interactif} (\codeinline{git rebase -i}) pour \enquote{nettoyer} son historique avant un \codeinline{git push}
    \item Le \alert{cherry-pick} (\codeinline{git cherry-pick}) pour reproduire un commit d'une branche à une autre
    \item Le \alert{test par bisection} (\codeinline{git bisect}) pour trouver quel \emph{commit} a introduit un bug
    \item Le \alert{blame} (\codeinline{git blame}) pour trouver quel commit a modifié une ligne
    \item Les \alert{hooks} pour faire des tests / exécuter du code lors de certaines actions
  \end{itemize}
\end{slide}
\subsection{Ressources en lignes}

\begin{slide}
  \nocite{*}
  \printbibliography[heading=none]
\end{slide}
\end{document}
