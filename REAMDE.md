# Présentation

Il s'agit d'un support de présentation pour une initiation à Git faite dans le cadre du stage LaTeX de Dunkerque.
# Dépendances
Le diaporama compile avec XeLaTeX. Il nécessite une TeXLive à jour. Il utiliser le package minted, lequel nécessite d'avoir [Pygments](http://pygments.org/) installé sur son ordinateur.

Pour compiler le plus simple est d'executer `latexmk`.

# Version compilée

On trouvera la [version compilée en ligne](https://geekographie.maieul.net/231).
